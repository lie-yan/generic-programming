/**
 *
 * @author: Lie Yan
 * @email: robin.lie.yan@outlook.com
 *
 * Origin: Stepanov's book - Elements of Programming
 */

#pragma once

#include "gn/type_functions.h"
#include "op_on_integral.h"

namespace eop
{

using namespace gn;

template<typename T>
// requires(CancellableMonoid(T))
T slow_remainder(T a, T b)
{
  // Precondition: a >= 0 and b > 0
  while (b <= a) a = a - b;
  return a;
}


template<typename T>
// requires(ArchimedeanMonoid(T))
QuotientType<T> slow_quotient(T a, T b)
{
  // Precondition: a >= 0 and b > 0
  QuotientType<T> n(0);
  while (b <= a) {
    a = a - b;
    n = successor(n);
  }
  return n;
}

template<typename T>
// requires(ArchimedeanMonoid(T))
T remainder_recursive(T a, T b)
{
  // Precondition: a >= b > 0
  if (a - b >= b) {
    a = remainder_recursive(a, b + b);
    if (a < b) return a;
  }
  return a - b;
}

template<typename T>
// requires(ArchimedeanMonoid(T))
T remainder_nonnegative(T a, T b)
{
  // Precondition: a >= 0 and b > 0
  if (a < b) return a;
  return remainder_recursive(a, b);
}

template<typename T>
// requires(ArchimedeanMonoid(T))
T largest_doubling(T a, T b)
{
  // Precondition: a >= b > 0
  while (b <= a - b) b = b + b;
  return b;
}

template<typename T>
// requires(HalvableMonoid(T))
T remainder_nonnegative_iterative(T a, T b)
{
  // Precondition: a >= 0 and b > 0
  if (a < b) return a;
  T c = largest_doubling(a, b);
  a = a - c;
  while (c != b) {
    c = half(c);
    if (c <= a) a = a - c;
  }
  return a;
}

template<typename T>
// requires(ArchimedeanMonoid(T))
T subtractive_gcd_nonzero(T a, T b)
{
  // Precondition: a > 0 and b > 0
  while (true) {
    if (b < a) a = a - b;
    else if (a < b) b = b - a;
    else return a;
  }
}


template<typename T>
// requires(EuclideanMonoid(T))
T subtractive_gcd(T a, T b)
{
  // Precondition: a >= 0 and b >= 0 and not(a = 0 and b = 0)
  while (true) {
    if (b == T(0)) return a;
    while (b <= a) a = a - b;
    if (a == T(0)) return b;
    while (a <= b) b = b - a;
  }
}

template<typename T>
// requires(EuclideanMonoid(T))
T fast_subtractive_gcd(T a, T b)
{
  // Precondition: a >= 0 and b >= 0 and not(a = 0 and b = 0)
  while (true) {
    if (b == T(0)) return a;
    a = remainder_nonnegative(a, b);
    if (a == T(0)) return b;
    b = remainder_nonnegative(b, a);
  }
}

template<typename T, typename S = void>
// requires(EuclideanSemiring(T) || EuclideanSemimodule(T, S))
T gcd(T a, T b)
{
  // Precondition: not(a= 0 and b = 0)
  while (true) {
    if (b == T(0)) return a;
    a = remainder(a, b);
    if (a == T(0)) return b;
    b = remainder(b, a);
  }
}

template<typename T>
// requires(ArchimedeanMonoid(T))
std::pair<QuotientType<T>, T> quotient_remainder_nonnegative(T a, T b)
{
  // Precondition: a >= 0 and b > 0
  using N = QuotientType<T>;
  if (a < b) return std::make_pair(N(0), a);
  if (a - b < b) return std::make_pair(N(1), a - b);
  std::pair<N, T> q = quotient_remainder_nonnegative(a, b + b);
  N               m = twice(q.first);
  a = q.second;
  if (a < b) return std::make_pair(m, a);
  else return std::make_pair(successor(m), a - b);
}

template<typename T>
// requires(HalvableMonoid(T))
std::pair<QuotientType<T>, T>
quotient_remainder_nonnegative_iterative(T a, T b)
{
  // Precondition: a >= 0 and b > 0
  using N = QuotientType<T>;
  if (a < b) return std::make_pair(N(0), a);
  T c = largest_doubling(a, b);
  a = a - c;
  N n(1);
  while (c != b) {
    n = twice(n);
    c = half(c);
    if (c <= a) {
      a = a - c;
      n = successor(n);
    }
  }
  return std::make_pair(n, a);
}

template<typename Op>
// requires(BinaryOperation(Op) && ArchimedeanGroup(Domain<Op>))
Domain<Op> remainder(Domain<Op> a, Domain<Op> b, Op rem)
{
  // Precondition: b != 0
  using T = Domain<Op>;
  T r;
  if (a < T(0)) {
    if (b < T(0)) {
      r = -rem(-a, -b);
    } else {
      r = rem(-a, b);
      if (r != T(0)) r = b - r;
    }
  } else {
    if (b < T(0)) {
      r = rem(a, -b);
      if (r != T(0)) r = b + r;
    } else {
      r = rem(a, b);
    }
  }
  return r;
}

template<typename F>
// requires(HomogeneousFunction(F) && Arity(F) == 2 &&
//          ArchimedeanGroup(Domain<F>) &&
//          Codomain<F> == pair<QuotientType<Domain<F>>, Domain<F>>)
std::pair<QuotientType<Domain<F>>, Domain<F> >
quotient_remainder(Domain<F> a, Domain<F> b, F quo_rem)
{
  // Precondition: b != 0
  using T = Domain<F>;
  std::pair<QuotientType<T>, T> q_r;
  if (a < T(0)) {
    if (b < T(0)) {
      q_r = quo_rem(-a, -b);
      q_r.second = -q_r.second;
    } else {
      q_r       = quo_rem(-a, b);
      if (q_r.second != T(0)) {
        q_r.second = b - q_r.second;
        q_r.first  = successor(q_r.first);
      }
      q_r.first = -q_r.first;
    }
  } else {
    if (b < T(0)) {
      q_r       = quo_rem(a, -b);
      if (q_r.second != T(0)) {
        q_r.second = b + q_r.second;
        q_r.first  = successor(q_r.first);
      }
      q_r.first = -q_r.first;
    } else
      q_r = quo_rem(a, b);
  }
  return q_r;
}
  
  
}