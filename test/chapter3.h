/**
 *
 * @author: Lie Yan
 * @email: robin.lie.yan@outlook.com
 *
 */

#pragma once

#include <gn/type_functions.h>
#include <ciso646>
#include "op_on_integral.h"

namespace eop
{

template<typename I, typename Op>
// requires(Integer(I) && BinaryOperation(Op))
Domain<Op> power_left_associated(const Domain<Op>& a, I n, Op op)
{
  // Precondition: n > 0
  if (n == I(1)) return a;
  
  Domain<Op> prod = a;
  n = n - I(1);
  do {
    prod = op(prod, a);
    n    = n - I(1);
  } while (n > I(0));
  return prod;
}

template<typename I, typename Op>
//requires(Integer(I) && BinaryOperation(Op))
Domain<Op> power_right_associated(const Domain<Op>& a, I n, Op op)
{
  // Precondition: n > 0
  if (n == I(1)) return a;
  
  Domain<Op> prod = a;
  n = n - I(1);
  do {
    prod = op(a, prod);
    n    = n - I(1);
  } while (n > I(0));
  return prod;
};


template<typename I, typename Op>
//requires(Integer(I) && BinaryOperation(Op))
Domain<Op> power_accumulate_positive(Domain<Op> r, Domain<Op> a, I n, Op op)
{
  // Precondition: associative(op) and positive(n)
  while (true) {
    if (odd(n)) {
      r = op(r, a);
      if (one(n)) return r;
    }
    a = op(a, a);
    n = half_nonnegative(n);
  }
}

template<typename I, typename Op>
//requires(Integer(I) && BinaryOperation(Op))
Domain<Op> power_accumulate(Domain<Op> r, Domain<Op> a, I n, Op op)
{
  // Precondition: associative(op) and not negative(n)
  if (zero(n)) return r;
  return power_accumulate_positive(r, a, n, op);
}

template<typename I, typename Op>
//requires(Integer(I) && BinaryOperation(Op))
Domain<Op> power(Domain<Op> a, I n, Op op)
{
  // Precondition: associative(op) and positive(n)
  while (even(n)) {
    a = op(a, a);
    n = half_nonnegative(n);
  }
  n = half_nonnegative(n);
  if (zero(n)) return a;
  return power_accumulate_positive(a, op(a, a), n, op);
}

template<typename I, typename Op>
//requires(Integer(I) && BinaryOperation(Op))
Domain<Op> power(Domain<Op> a, I n, Op op, Domain<Op> id)
{
  // Precondition: associative(op) and not negative(n)
  if (zero(n)) return id;
  return power(a, n, op);
}


template<typename I>
//requires(Integer(I))
std::pair<I, I> fibonacci_matrix_multiply(const std::pair<I, I>& x,
                                          const std::pair<I, I>& y)
{
  return std::make_pair(x.first * (y.second + y.first) + x.second * y.first,
                        x.first * y.first + x.second * y.second);
}

template<typename I>
//requires(Integer(I))
I fibonacci(I n)
{
  // Precondition: n >= 0
  if (n == I(0)) return I(0);
  return power(std::make_pair(I(1), I(0)),
               n,
               fibonacci_matrix_multiply<I>).first;
}
  
}