/**
 *
 * @author: Lie Yan
 * @email: robin.lie.yan@outlook.com
 *
 * Origin: Stepanov's book - Elements of Programming
 */

#pragma once

#include "gn/type_functions.h"
#include "gn/pointer.h"
#include <ciso646>

namespace eop
{

using namespace gn;

template<typename I>
// requires(Iterator(I))
void increment(I& x)
{
  // Precondition: successor(x) is defined
  x++;
}

template<typename I>
// requires(Iterator(I))
I operator +(I f, DistanceType<I> n)
{
  // Precondition: n >= 0 and weak_range(f, n)
  while (!zero(n)) {
    n = predecessor(n);
    f++;
  }
  return f;
}

template<typename I, typename Proc>
// requires(Readable(I) && Iterator(I) &&
//          Procedure(Proc) && Arity(Proc) == 1 &&
//          ValueType(I) == UndecoratedInputType(Proc, 0))
Proc for_each(I f, I l, Proc proc)
{
  // Precondition: readable_bounded_range(f, l)
  while (f != l) {
    proc(*f);
    f++;
  }
  return proc;
}

template<typename I>
// requires(Readable(I) && Iterator(I))
I find(I f, I l, const ValueType<I>& x)
{
  // Precondition: readable_unbounded_range(f,l)
  while (f != l && *f != x) f++;
  return f;
}

template<typename I, typename P>
// requires(Readable(I) && Iterator(I) &&
//          UnaryPredicate(P) && ValueType(I) == Domain(P))
I find_if(I f, I l, P p)
{
  // Precondition: readable_bounded_range(f, l)
  while (f != l and !p(*f)) f++;
  return f;
}

template<typename I, typename P, typename J>
// requires(Readable(I) && Iterator(I) &&
//          UnaryPredicate(P) && Iterator(J) &&
//          ValueType(I) == Domain(P))
J count_if(I f, I l, P p, J j)
{
  // Precondition: readable_bounded_range(f, l)
  while (f != l) {
    if (p(*f)) j++;
    f = ++f;
  }
  return j;
}


template<typename I, typename P>
// requires(Readable(I) && Iterator(I) &&
//          UnaryPredicate(P) && ValueType(I) == Domain(P))
DistanceType<I> count_if(I f, I l, P p)
{
  // Precondition: readable_bounded_range(f, l)
  return count_if(f, l, p, DistanceType<I>(0));
}

template<typename I, typename Op, typename F>
// requires(Iterator(I) && BinaryOperation(Op) &&
//          UnaryFunction(F) && I == Domain(F) && Codomain(F) == Domain(Op)
Domain<Op> reduce_nonempty(I f, I l, Op op, F fun)
{
  // Precondition: bounded_range(f, l) and f != l
  // Precondition: partially_associative(op)
  // Precondition: (forall x in [f, l) fun(x) is defined
  Domain<Op> r = fun(f);
  f++;
  while (f != l) {
    r = op(r, fun(f));
    f++;
  }
  return r;
};

template<typename I, typename Op, typename F>
// requires(Iterator(I) && BinaryOperation(Op) &&
//          UnaryFunction(F) && I == Domain(F) && Codomain(F) == Domain(Op))
Domain<Op> reduce(I f, I l, Op op, F fun, const Domain<Op>& z)
{
  // Precondition: bounded_range(f, l)
  // Precondition: partially_associative(op)
  // Precondition: (forall x\in[f,l)) fun(x) is defined
  if (f == l) return z;
  return reduce_nonempty(f, l, op, fun);
}

template<typename I, typename Op, typename F>
// requires(Iterator(I) && BinaryOperation(Op) &&
//          UnaryFunction(F) && I == Domain(F) && Codomain(F) == Domain(Op)
Domain<Op> reduce_nonzeroes(I f, I l, Op op, F fun, const Domain<Op>& z)
{
  // Precondition: bounded_range(f, l)
  // Precondition: partially_associative(op)
  // Precondition: (forall x \in [f, l)) fun(x) is defined
  Domain<Op> x;
  do {
    if (f == l) return z;
    x = fun(f);
    f++;
  } while (x == z);
  while (f != l) {
    Domain<Op> y = fun(f);
    if (y != z) x = op(x, y);
    f++;
  }
  return x;
}

template<typename I, typename Proc>
// requires(Readable(I) && Iterator(I) &&
//          Procedure(Proc) && Arity(Proc) == 1 &&
//          ValueType(I) == InputType(Proc, 0))
std::pair<Proc, I> for_each_n(I f, DistanceType<I> n, Proc proc)
{
  // Precondition: readable_weak_range(f, n)
  while (!zero(n)) {
    n--;
    proc(*f);
    f++;
  }
  return std::make_pair(proc, f);
}

template<typename I>
// requires(Readable(I) && Iterator(I))
std::pair<I, DistanceType<I>> find_n(I f, DistanceType<I> n, const ValueType<I>& x)
{
  // Precondition: readable_weak_range(f, n)
  while (!zero(n) && *f != x) {
    n--;
    f++;
  }
  return std::make_pair(f, n);
}

template<typename I, typename P>
// requires(Readable(I) && Iterator(I) &&
//          UnaryPredicate(P) && ValueType(I) == Domain(P))
I find_if_unguarded(I f, P p)
{
  // Precondition: (exists l) readable_bounded_range(f, l) and some(f, l, p)
  while (!p(*f)) f++;
  return f;
}

template<typename I0, typename I1, typename R>
// requires(Readable(I0) && Iterator(I0) &&
//          Readable(I1) && Iterator(I1) && Relation(R) &&
//          ValueType(I0) == ValueType(I1) &&
//          ValueType(I0) == Domain(R))
std::pair<I0, I1> find_mismatch(I0 f0, I0 l0, I1 f1, I1 l1, R r)
{
  // Precondition: readable_bounded_range(f0, l0)
  // Precondition: readable_bounded_range(f1, l1)
  while (f0 != l0 && f1 != l1 && r(*f0, *f1)) {
    f0++;
    f1++;
  }
  return std::make_pair(f0, f1);
}


template<typename I, typename R>
// requires(Readable(I) && Iterator(I) &&
//          Relatioin(R) && ValueType(I) == Domain(R))
I find_adjacent_mismatch(I f, I l, R r)
{
  // Precondition: readable_bounded_range(f, l)
  if (f == l) return l;
  ValueType<I> x = *f;
  f++;
  while (f != l && r(x, *f)) {
    x = *f;
    f++;
  }
  return f;
}

template<typename I, typename R>
// requires(Readable(I) && Iterator(I) &&
//          Relation(R) && ValueType(I) == Domain(R))
bool relation_preserving(I f, I l, R r)
{
  // Precondition: readable_bounded_range(f, l)
  return l == find_adjacent_mismatch(f, l, r);
}

template<typename I, typename R>
// requires(Readable(I) && Iterator(I) &&
//          Relation(R) && ValueType(I) == Domain(R))
bool strictly_increasing_range(I f, I l, R r)
{
  // Precondition: readable_bounded_range(f,l) and weak_ordering(r)
  return relation_preserving(f, l, r);
}

template<typename R>
// requires(Relation(R))
struct complement_of_converse
{
  using T = Domain<R>;
  R r;
  complement_of_converse(const R& r) : r(r) {}
  bool operator ()(const T& a, const T& b)
  {
    return !r(b, a);
  }
};

template<typename I, typename R>
// requires(Readable(I) && Iterator(I) &&
//          Relation(R) && ValueType(I) == Domain(R))
bool increasing_range(I f, I l, R r)
{
  // Precondition: readable_bounded_range(f,l) && weak_ordering(r)
  return relation_preserving(f, l, complement_of_converse<R>(r));
}

template<typename I, typename P>
// requires(Readable(I) && Iterator(I) &&
//          UnaryPredicate(P) && ValueType(I) == Domain(P))
bool partitioned(I f, I l, P p)
{
  // Precondition: readable_bounded_range(f,l)
  return l == std::find_if_not(eop::find_if(f, l, p), l, p);
}

template<typename I, typename R>
// requires(Readable(I) && ForwardIterator(I) &&
//          Relation(R) && ValueType(I)==Domain(R))
I find_adjacent_mismatch_forward(I f, I l, R r)
{
  // Precondition: readable_bounded_range(f,l)
  if (f == l) return l;
  I t;
  do {
    t = f;
    f++;
  } while (f != l and r(*t, *f));
  return f;
}

template<typename I, typename P>
// requires(Readable(I) && ForwardIterator(I) &&
//          UnaryPredicate(P) && ValueType(I) == Domain(P))
I partition_point_n(I f, DistanceType<I> n, P p)
{
  // Precondition: readable_counted_range(f,n) and partitioned_n(f, n, p)
  while (!zero(n)) {
    DistanceType<I> h = half_nonnegative(n);
    I               m = f + h;
    if (p(*m)) {
      n = h;
    } else {
      n = n - (h + 1);
      f = std::next(m);
    }
  }
  return f;
}

template<typename I, typename P>
// requires(Readable(I) && ForwardIterator(I) &&
//          UnaryPredicate(P) && ValueType(I)==Domain(P))
I partition_point(I f, I l, P p)
{
  // Precondition: readable_bounded_range(f,l) and partitioned(f,l,p)
  return partition_point_n(f, l - f, p);
}

template<typename R>
// requires(Relation(R))
struct lower_bound_predicate
{
  using T = Domain<R>;
  const T& a;
  R r;
  lower_bound_predicate(const T& a, R r) : a(a), r(r) {}
  bool operator ()(const T& x) { return !r(x, a); }
};

template<typename I, typename R>
// requires(Readable(I) && ForwardIterator(I) &&
//          Relation(R) && ValueType(I) == Domain(R))
I lower_bound_n(I f, DistanceType<I> n, const ValueType<I>& a, R r)
{
  // Precondition: weak_ordering and incresing_counted_range(f, n, r)
  lower_bound_predicate<R> p(a, r);
  return partition_point_n(f, n, p);
}

template<typename R>
// requires(Relation(R))
struct upper_bound_predicate
{
  using T = Domain<R>;
  const T& a;
  R r;
  upper_bound_predicate(const T& a, R r) : a(a), r(r) {}
  bool operator ()(const T& x) { return r(a, x); }
};

template <typename I, typename R>
// requires(Readable(I) && ForwardIterator(I) &&
//          Relation(R) && ValueType(I) == Domain(R))
I upper_bound_n(I f, DistanceType<I> n, const ValueType<I>& a, R r)
{
  upper_bound_predicate<R> p(a, r);
  return partition_point_n(f, n, p);
}

template <typename I>
// requires(BidirectionalIterator(I))
I operator-(I l, DistanceType<I> n)
{
  // Precondition: n >= 0 and (exists f\in I) weak_range(f,n) and l=f+n
  while (! zero(n)) {
    --n;
    --l;
  }
  return l;
}

template <typename I, typename P>
// requires(Readable(I) && BidirectionalIterator(I) &&
//          UnaryPredicate(P) && ValueType(I) == Domain(P))
I find_backward_if(I f, I l, P p)
{
  // Precondition: (f, l] is a readable bounded half-open on left range
  while (l != f && !p(*std::prev(l))
    l--;
  return l;
}
  
}