/**
 *
 * @author: Lie Yan
 * @email: robin.lie.yan@outlook.com
 *
 */

#pragma once

#include <gn/type_functions.h>
#include <gn/intrinsics.h>
#include <functional>
#include <ciso646>

namespace eop
{

using namespace gn;

template<typename Op>
//requires(BinaryOperation(Op))
Domain<Op> square(const Domain<Op>& x, Op op)
{
  return op(x, x);
}

template<typename F, typename N>
//requires(Transformation(F) && Integer(N))
Domain<F> power_unary(Domain<F> x, N n, F f)
{
  while (n != N(0)) {
    n = n - N(1);
    x = f(x);
  }
  return x;
};

template<typename F>
//requires(Transformation(F))
DistanceType<Domain<F>> distance(Domain<F> x, Domain<F> y, F f)
{
  using N = DistanceType<Domain<F>>;
  N n(0);
  while (x != y) {
    x = f(x);
    n = n + N(1);
  }
  return n;
}

template<typename F, typename P>
//requires(Transformation(F) && UnaryPredicate(P) && Domain<F> == Domain<P>)
Domain<F> collision_point(const Domain<F>& x, F f, P p)
{
  // Precondition: p(x) <=> f(x) is defined
  if (!p(x)) return x;
  
  Domain<F> slow = x;
  Domain<F> fast = f(x);
  while (fast != slow) {
    slow = f(slow);
    if (!p(fast)) return fast;
    fast = f(fast);
    if (!p(fast)) return fast;
    fast = f(fast);
  }
  return fast;
  // Postcondition: return value is terminal point or collision point
};

template<typename F, typename P>
//requires(Transformation(F) && UnaryPredicate(P) && Domain<F> == Domain<P>)
bool terminating(const Domain<F>& x, F f, P p)
{
  // Precondition: p(x) <=> f(x) is defined
  return !p(collision_point(x, f, p));
};

template<typename F>
//requires(Transformation(F))
Domain<F> collision_point_nonterminating_orbit(const Domain<F>& x, F f)
{
  Domain<F> slow = x;
  Domain<F> fast = f(x);
  
  while (fast != slow) {
    slow = f(slow);
    fast = f(fast);
    fast = f(fast);
  }
  return fast;
}

template<typename F>
//requires(Transformation(F))
bool circular_nonterminating_orbit(const Domain<F>& x, F f)
{
  return x == f(collision_point_nonterminating_orbit(x, f));
}

template<typename F, typename P>
//requires(Transformation(F) && UnaryPredicate(P) && Domain<F> == Domain<P>)
bool circular(const Domain<F>& x, F f, P p)
{
  // Precondition: p(x) <=> f(x) is defined
  Domain<F> y = collision_point(x, f, p);
  return p(y) and x == f(y);
};

template<typename F>
//requires(Transformation(F))
Domain<F> convergent_point(Domain<F> x0, Domain<F> x1, F f)
{
  // Precondition: $(\exists n\in DistanceType<F>) n \ge 0 \land f^n(x0) = f^n(x1)$
  while (x0 != x1) {
    x0 = f(x0);
    x1 = f(x1);
  }
  return x0;
}

template<typename F>
//requires(Transformation(F))
Domain<F> connection_point_nonterminating_orbit(const Domain<F>& x, F f)
{
  return convergent_point(
      x,
      f(collision_point_nonterminating_orbit(x, f)),
      f);
}

template<typename F, typename P>
//requires(Transformation(F) && UnaryPredicate(P) && Domain<F> == Domain<P>)
Domain<F> connection_point(const Domain<F>& x, F f, P p)
{
  // Precondition: p(x) <=> f(x) is defined
  Domain<F> y = collision_point(x, f, p);
  if (!p(y)) return y;
  return convergent_point(x, f(y), f);
};

template<typename F>
//requires(Transformation(F))
std::tuple<DistanceType<F>, DistanceType<F>, Domain<F> >
orbit_structure_nonterminating_orbit(const Domain<F>& x, F f)
{
  Domain<F> y = connection_point_nonterminating_orbit(x, f);
  return std::make_tuple(distance(x, y, f),
                         distance(f(y), y, f),
                         y);
}

template<typename F, typename P>
//requires(Transformation(F) && UnaryPredicate(P) && Domain<F> == Domain<P>)
std::tuple<DistanceType<Domain<F>>, DistanceType<Domain<F>>, Domain<F> >
orbit_structure(const Domain<F>& x, F f, P p)
{
  // Precondition: p(x) <=> f(x) is defined
  using N = DistanceType<Domain<F>>;
  Domain<F> y = connection_point(x, f, p);
  N         m = distance(x, y, f);
  N         n(0);
  if (p(y)) n = distance(f(y), y, f);
  return std::make_tuple(m, n, y);
}
  
}
