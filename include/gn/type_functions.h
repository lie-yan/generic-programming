/**
 *
 * @author: Lie Yan
 * @email: robin.lie.yan@outlook.com
 *
 */

#pragma once

#include <type_traits>
#include <cstddef>
#include <iterator>
#include "callable_traits.h"

namespace gn
{

template<typename T>
using pointer = T*;



template<typename T, typename Enable = void>
struct value_type
{
  using type = typename T::value_type;
};

template<typename T>
struct value_type<pointer<T>>
{
  using type = T;
};

template<typename T>
struct value_type<T, typename std::enable_if<std::is_arithmetic<T>::value>::type>
{
  using type = T;
};

template<typename T>
using ValueType = typename value_type<T>::type;

template<typename F, typename Enable=void>
struct distance_type;

template<typename T>
//requires(Regular(T))
struct distance_type<pointer<T>>
{
  using type = ptrdiff_t;
};

template<typename T>
//requires(SignedIntegral(T))
struct distance_type<T, std::enable_if_t<std::is_integral<T>::value
                                         && std::is_signed<T>::value>>
{
  using type = std::make_unsigned_t<T>;
};

template<typename I>
//requires(Iterator(I))
struct distance_type<I, std::enable_if_t<std::is_class<I>::value>>
{
  using type = typename I::difference_type;
};

template<typename T>
using DistanceType = typename distance_type<T>::type;


template<typename T>
struct quotient_type;

template<>
struct quotient_type<int>
{
  using type = int;
};

template<>
struct quotient_type<long>
{
  using type = long;
};

template<>
struct quotient_type<double>
{
  using type = long;
};

template<typename T>
using QuotientType = typename quotient_type<T>::type;
  
  
}



